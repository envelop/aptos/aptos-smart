#[test_only]
module tech_nft_address::tech_nft_tests {
    use tech_nft_address::tech_nft;
    use aptos_framework::guid;

    struct GUID has drop {
            id: guid::ID 
        }
    // ------------------------------------ tests ------------------------------------


    #[test(framework = @0x1, _admin = @admin, _source = @source, resource_acc = @0x8e8439def8e9900ba0c26b2d36520deaba524a27b8eb0c4e445dcd975c60d814, buyer = @0x1234)]
    public entry fun normal_process(framework: &signer, _admin: &signer, _source: &signer, resource_acc: &signer, buyer: &signer)  {

        tech_nft::test_setup(_source, resource_acc, framework, buyer);

//        tech_nft::issue_collection(_admin);
    
    }

}