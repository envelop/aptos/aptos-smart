module tech_nft_address::tech_nft {
    use std::error;
    use std::signer;
    use std::string;
    use std::vector;
    use std::option;
    use aptos_framework::account;
    use aptos_framework::event;
    use aptos_framework::resource_account;
    use aptos_framework::object;
    use aptos_token_objects::collection;
    use aptos_token_objects::token;


    // Errors
    const ENOT_ADMIN: u64 = 0;
    const EMINTING_DISABLED: u64 = 1;
    const ENO_SUFFICIENT_FUND: u64 = 2;

    // Constants
    const COLLECTION_NAME: vector<u8> = b"Aptos NFT from DAO Envelop";
    const COLLECTION_URI: vector<u8> = b"https://api.envelop.is/metadata/100000001/0x12345678/";
    const COLLECTION_SUPPLY: u64 = 0;
    const TOKEN_NAME: vector<u8> = b"ANE #";
    const TOKEN_URI: vector<u8> = b"https://api.envelop.is/metadata/100000001/0x12345678/";
    const TOKEN_SUPPLY: u64 = 1;
    const DESCRIPTION: vector<u8> = b"";

    // Resources
    struct TokenMintingEvent has drop, store {
        minter_addr: address,
    }

    struct TechNFTTokenData has key {
        signer_cap: account::SignerCapability,
        admin_addr: address,
        minted_supply: u64,
		collection_address: address,
        token_minting_events: event::EventHandle<TokenMintingEvent>,
    }


    fun assert_is_admin(addr: address) acquires TechNFTTokenData {
        let admin = borrow_global<TechNFTTokenData>(@tech_nft_address).admin_addr;
        assert!(addr == admin, error::permission_denied(ENOT_ADMIN));
    }

    fun init_module(resource_acc: &signer)  acquires TechNFTTokenData {

        let signer_cap = resource_account::retrieve_resource_account_cap(resource_acc, @source);

        move_to(resource_acc, TechNFTTokenData {
            signer_cap,
            admin_addr: @admin,
            minted_supply: 0,
			collection_address: @0x0,
            token_minting_events: account::new_event_handle<TokenMintingEvent>(resource_acc),
        });
        issue_collection(resource_acc);
    }


    // Admin-only functions

    fun issue_collection(creator: &signer) acquires TechNFTTokenData {
//        assert_is_admin(signer::address_of(creator));

        let minter_resource = borrow_global_mut<TechNFTTokenData>(@tech_nft_address);
        let resource_signer = account::create_signer_with_capability(&minter_resource.signer_cap);

        let collection_construction_ref = collection::create_unlimited_collection(
            &resource_signer,
            string::utf8(DESCRIPTION),
            string::utf8(COLLECTION_NAME),
            option::none(),
            string::utf8(COLLECTION_URI),
        );
		let collection_address = object::address_from_constructor_ref(&collection_construction_ref);
		minter_resource.collection_address = collection_address;
    }

    public entry fun set_admin(admin: &signer, admin_addr: address) acquires TechNFTTokenData {
        let addr = signer::address_of(admin);
        assert_is_admin(addr);
        borrow_global_mut<TechNFTTokenData>(@tech_nft_address).admin_addr = admin_addr;
    }

    public entry fun mint_nft(buyer: &signer) acquires TechNFTTokenData {
        /*let buyer_addr = signer::address_of(buyer);
        let minter_resource = borrow_global_mut<TechNFTTokenData>(@tech_nft_address);
        minter_resource.minted_supply = minter_resource.minted_supply + 1;
        let resource_signer = account::create_signer_with_capability(&minter_resource.signer_cap);
        
        let name = string::utf8(TOKEN_NAME);
        string::append(&mut name, u64_to_string(minter_resource.minted_supply));

        let uri = string::utf8(TOKEN_URI);
        string::append(&mut uri, u64_to_string(minter_resource.minted_supply));
        string::append_utf8(&mut uri, b".json");

        let constructor_ref = token::create_from_account(
        &resource_signer,
        string::utf8(COLLECTION_NAME),
        string::utf8(b""),
        name,
        option::none(),
        uri,
        );


        // Transfers the token.
        let transfer_ref = object::generate_transfer_ref(&constructor_ref);
        let linear_transfer_ref = object::generate_linear_transfer_ref(&transfer_ref);
        object::transfer_with_ref(linear_transfer_ref, buyer_addr);
        
        //let minted_token_objet = object::object_from_constructor_ref<Token>(&constructor_ref)
        object::address_from_constructor_ref(&constructor_ref)
		*/
        mint_nft_address(buyer);
    }

    public fun mint_nft_address(buyer: &signer): address acquires TechNFTTokenData {
        let buyer_addr = signer::address_of(buyer);
        let minter_resource = borrow_global_mut<TechNFTTokenData>(@tech_nft_address);
        minter_resource.minted_supply = minter_resource.minted_supply + 1;
        let resource_signer = account::create_signer_with_capability(&minter_resource.signer_cap);
        
        let name = string::utf8(TOKEN_NAME);
        string::append(&mut name, u64_to_string(minter_resource.minted_supply));

        let uri = string::utf8(TOKEN_URI);
        string::append(&mut uri, u64_to_string(minter_resource.minted_supply));
//        string::append_utf8(&mut uri, b".json");

        let constructor_ref = token::create_from_account(
        &resource_signer,
        string::utf8(COLLECTION_NAME),
        string::utf8(b""),
        name,
        option::none(),
        uri,
        );


        // Transfers the token.
        let transfer_ref = object::generate_transfer_ref(&constructor_ref);
        let linear_transfer_ref = object::generate_linear_transfer_ref(&transfer_ref);
        object::transfer_with_ref(linear_transfer_ref, buyer_addr);
        
        //let minted_token_objet = object::object_from_constructor_ref<Token>(&constructor_ref)
        object::address_from_constructor_ref(&constructor_ref)
		
    }

	#[view]
	public fun get_collection_address(): address acquires TechNFTTokenData {
    	let minter_resource = borrow_global<TechNFTTokenData>(@tech_nft_address);
		minter_resource.collection_address
	}
    
    fun u64_to_string(value: u64): string::String {
        if (value == 0) {
            return string::utf8(b"0")
        };
        let buffer = vector::empty<u8>();
        while (value != 0) {
            vector::push_back(&mut buffer, ((48 + value % 10) as u8));
            value = value / 10;
        };
        vector::reverse(&mut buffer);
        string::utf8(buffer)
    }

    // ------------------------------------ tests ------------------------------------

    #[test_only]
    use aptos_framework::aptos_account::create_account;
    use aptos_framework::guid;
    use std::bcs;
    use aptos_std::from_bcs;
    use std::hash;

    struct GUID has drop {
            id: guid::ID 
        }

    public fun create_address_for_last_object_account( source: &signer):address   {

        let addr = signer::address_of(source);

        let creation_num = account::get_guid_next_creation_num(addr)-1;
        let guid_id = guid::create_id(addr, creation_num);
        let guid = GUID {
            id: guid_id
        };


        let bytes = bcs::to_bytes(&guid);
        let object_from_guid_address_scheme: u8 = 0xFD; 
        vector::push_back(&mut bytes, object_from_guid_address_scheme);
        let obj_addr = from_bcs::to_address(hash::sha3_256(bytes));

        obj_addr
    }

    #[test_only]
    public fun test_setup(source: &signer, resource_acc: &signer, _framework: &signer, buyer: &signer)  acquires TechNFTTokenData{

        let addr_source = signer::address_of(source);
        let addr_buyer = signer::address_of(buyer);
        
        if (!account::exists_at(addr_source)) {
        create_account(signer::address_of(source));
        };
        if (!account::exists_at(addr_buyer)) {
        create_account(signer::address_of(buyer));
        };

        resource_account::create_resource_account(source, (vector[1u8]: vector<u8>), vector::empty());

        init_module(resource_acc);
    }


    #[test_only]
    public entry fun print_address_resource_acc( _source: &signer)   {

        let resource_addr = account::create_resource_address(&signer::address_of(_source), (vector[1u8]: vector<u8>));
        aptos_std::debug::print<address>(&resource_addr);
        
    }
    



    #[test(framework = @0x1, _admin = @admin, _source = @source, resource_acc = @0x8e8439def8e9900ba0c26b2d36520deaba524a27b8eb0c4e445dcd975c60d814, buyer = @0x1234)]
    public entry fun normal_process(framework: &signer, _admin: &signer, _source: &signer, resource_acc: &signer, buyer: &signer)  acquires TechNFTTokenData {
        print_address_resource_acc( _source);
        test_setup(_source, resource_acc, framework, buyer);

        exists<TechNFTTokenData>(@tech_nft_address);
//        issue_collection(_admin);
    
    }

}