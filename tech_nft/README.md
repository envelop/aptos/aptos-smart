### Deploy
```
aptos move create-resource-account-and-publish-package --address-name  tech_nft_address {--seed X}
```
{--seed X} - optional. It is need when deploy nore than one contract from one user account. 
For this nft contact, recomended {--seed 1}, for main contract {--seed 2}
### Test
```
aptos move test --named-addresses tech_nft_address=10bf0bbc84fc8f5306da1f2bb56132985d90db7c42221e79e257c6dc55c07280
```
