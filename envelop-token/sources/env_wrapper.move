module env_address::env_wrapper {
    use std::error;
    use std::signer;
    use std::string::{Self, String, utf8};
    use std::vector;
    use std::option;

    use aptos_framework::aptos_coin::{AptosCoin};
    use aptos_framework::account;
    use aptos_framework::coin;
    use aptos_framework::event;
    use aptos_framework::resource_account;
    use aptos_framework::object::{Self, Object};
    use aptos_framework::fungible_asset;
    use aptos_framework::primary_fungible_store;
    use aptos_token_objects::collection;
    use aptos_token_objects::token::{Self, Token};
   
	use aptos_framework::timestamp;
    use std::simple_map::{SimpleMap,Self};
    use std::bcs;
    use aptos_std::from_bcs;
    use std::hash;
    use aptos_framework::guid;
    use tech_nft_address::tech_nft;

    // Errors
    const ENOT_ADMIN: u64 = 0;
    const EMINTING_DISABLED: u64 = 1;
    const ENO_SUFFICIENT_FUND: u64 = 2;
    const ECOLLECTION_NOT_FOUND: u64 = 3;
    const ETOKEN_ID_NOT_FOUND: u64 = 4;
    const ETOO_LONG_WRAP: u64 = 5;
    const ECHECK_WRAP_DATA: u64 = 6;
    const EBLACKLISTED_FOR_WRAP: u64 =7;
    const EBAD_TIME_TO_UNWRAP: u64 =8;
    const ELOW_FA_BALANCE: u64 =9;
    const ETOO_MUCH_COLLATERIAL: u64 =10;
    const ECOLLECTION_IN_BLACKLIST: u64 =11;
    const ECOIN_NOT_ENOUGH: u64 =12;
    const ECHECK_NATIVE_COLLATERAL_EVENT: u64 =13;
    const ECHECK_FA_COLLATERAL_EVENT: u64 =14;
    
    


    const MAX_FA_COUNT: u64 = 25;
	
	
	const MAX_TIME_TO_UNWRAP: u64 = 100*365*24*60*60;


    // Resources
    struct TokenMintingEvent has drop, store {
        minter_addr: address,
    }

    #[event]
    struct NativeCollateralAdd has drop, store {
        token: address,
        add: u64,
        result: u64,
    }

    #[event]
    struct PartnersListChange has drop, store {
        contract: address,
        enabled_for_collateral: bool,
        disabled_for_wrap: bool,
    }

    #[event]
    struct FACollateralAdd has drop, store {
        token: address,
        metadata: address,
        add: u64,
        result: u64,
    }

    #[event]
    struct FACollateralStatusChanged has drop, store {
        metadata: address,
        newStatus: bool,
    }

    #[event]
    struct MaxCollateralCountChanged has drop, store {
        oldValue: u64,
        newValue: u64,
    }


    struct GUID has drop {
            id: guid::ID 
        
        }
   struct BurnConfig has key {
		burn_ref: token::BurnRef,
   }

    struct ListItem has store, copy, drop { 
        enabled_for_collateral: bool,
        disabled_for_wrap: bool,
    }

    struct WTokenData has store, copy, drop {
        addr: address,
        unwrap_after: u64,
        backed_value: u64,
   }
   struct FaCollateral has store, copy, drop {
        metadata_address: address,
        amount: u64
    }

    struct EnvWrapperData has key {
        signer_cap: account::SignerCapability,
        admin_addr: address,
        minted_supply: u64,
        token_minting_events: event::EventHandle<TokenMintingEvent>,
        creator: address,
        collection_name: String,
        collection_description: String,
        collection_uri: String,
        collection_mutate_config: vector<bool>,
        token_name_base: String,
        token_description: String,
        partners_token_list: SimpleMap<address, ListItem>,
        max_fa_count: u64
    }

    struct EnvWrapperDataMut has key {
        token_relation: SimpleMap<address, WTokenData>,
        collaterial_relation: SimpleMap<address, vector<FaCollateral>>
    }

    fun assert_is_admin(addr: address) acquires EnvWrapperData {
        let admin = borrow_global<EnvWrapperData>(@env_address).admin_addr;
        assert!(addr == admin, error::permission_denied(ENOT_ADMIN));
    }

    fun init_module(resource_acc: &signer) acquires EnvWrapperData {
        // creates signer from resource account and store EnvWrapperData under Resource acc
        let signer_cap = resource_account::retrieve_resource_account_cap(resource_acc, @source);
        let resource_signer = account::create_signer_with_capability(&signer_cap);
        
        move_to(resource_acc, EnvWrapperData {
            signer_cap,
            admin_addr: @admin,
            minted_supply: 0,
            token_minting_events: account::new_event_handle<TokenMintingEvent>(resource_acc),
            creator: signer::address_of(&resource_signer),
            collection_name: utf8(b"Envelop wrap tokens collection"),
            collection_description: utf8(b"Envelop project collection, all tokens are wrapped"),
            collection_uri: utf8(b"https://api.envelop.is/metadata/2000000001/"),
            collection_mutate_config: vector<bool>[ false, false, false ],
            token_name_base: utf8(b"Envelop wraped token "),
            token_description: utf8(b"Envelop wraped token "),
            partners_token_list: simple_map::create(),
            max_fa_count: MAX_FA_COUNT
        });

        move_to(resource_acc, EnvWrapperDataMut {
            token_relation: simple_map::create(),
            collaterial_relation: simple_map::create()
        });
        issue_collection(&resource_signer);
        
		coin::register<AptosCoin>(resource_acc);
    }

    public entry fun wrap(
        token_owner: &signer, 
        token_to_wrap_address: address,
        unwrap_after: u64,
		value: u64,
    )
        acquires EnvWrapperData, EnvWrapperDataMut {
        let token_owner_address = signer::address_of(token_owner);
		assert!(unwrap_after <= timestamp::now_seconds() + MAX_TIME_TO_UNWRAP,error::invalid_argument(ETOO_LONG_WRAP));
		assert!(coin::balance<AptosCoin>(token_owner_address) >= value ,error::invalid_argument(ENO_SUFFICIENT_FUND));

        let token_to_wrap = object::address_to_object<Token>(token_to_wrap_address); 
        let env_wrapper_data = borrow_global<EnvWrapperData>(@env_address);

//chek collection

        let collection_token_to_wrap = token::collection_object<Token>(token_to_wrap);
        let collection_token_to_wrap_address = object::object_address<collection::Collection>(&collection_token_to_wrap);

		let tech_token_collection_address = tech_nft::get_collection_address();

		
		if (tech_token_collection_address != collection_token_to_wrap_address)  {
            if (simple_map::contains_key(&env_wrapper_data.partners_token_list, &collection_token_to_wrap_address)) {
                let partners_token_list = *simple_map::borrow(&env_wrapper_data.partners_token_list, &collection_token_to_wrap_address);
                assert!(!partners_token_list.disabled_for_wrap,error::invalid_argument(EBLACKLISTED_FOR_WRAP));
            }
		};
		
    
        
        //move wrapped token to resource account
        let resource_signer = account::create_signer_with_capability(&env_wrapper_data.signer_cap);
        object::transfer(token_owner, token_to_wrap, env_wrapper_data.creator);

		if (value>0) coin::transfer<AptosCoin>(token_owner, env_wrapper_data.creator, value);
        //mint new token
        let wrapped_token = mint_nft(token::uri<Token>(token_to_wrap));


        let wrapped_token_address = object::object_address<Token>(&wrapped_token);
        //move new token to token_owner
        
        object::transfer(&resource_signer, wrapped_token, token_owner_address);
        
        //store in map relation wrapped token and new token


        let env_wrapper_data_mut = borrow_global_mut<EnvWrapperDataMut>(@env_address);
        let w_token_data = WTokenData  {
			addr: token_to_wrap_address,
			unwrap_after,
			backed_value: value,
   		};
        simple_map::add(&mut env_wrapper_data_mut.token_relation, wrapped_token_address, w_token_data); 
    }


    public entry fun wrap_with_mint(
        token_owner: &signer, 
        unwrap_after: u64,
		value: u64,
    )
    acquires EnvWrapperData, EnvWrapperDataMut {
    let minted_token_address = tech_nft_address::tech_nft::mint_nft_address(token_owner);
    wrap(
        token_owner, 
        minted_token_address,
        unwrap_after,
		value
    );
    }


    public entry fun unwrap(
        token_owner: &signer,
        token_to_umwrap_address: address)
        acquires EnvWrapperData, EnvWrapperDataMut, BurnConfig {
        let env_wrapper_data_mut = borrow_global_mut<EnvWrapperDataMut>(@env_address);
        let env_wrapper_data = borrow_global<EnvWrapperData>(@env_address);

        let w_token_data = *simple_map::borrow(&mut env_wrapper_data_mut.token_relation, &token_to_umwrap_address);


        //check token ownership
        let token_to_unwrap = object::address_to_object<Token>(token_to_umwrap_address); 
        let token_owner_address = signer::address_of(token_owner);
        
        // Time lock check
		assert!(w_token_data.unwrap_after <= timestamp::now_seconds(),error::invalid_argument(EBAD_TIME_TO_UNWRAP));
        
        
        //move wrapped token to owner account
        let wrapped_token = object::address_to_object<Token>(w_token_data.addr); 
        let resource_signer = account::create_signer_with_capability(&env_wrapper_data.signer_cap);
        
        object::transfer(&resource_signer, wrapped_token, token_owner_address);
		if (w_token_data.backed_value>0) coin::transfer<AptosCoin>(&resource_signer, token_owner_address, w_token_data.backed_value);
                
        return_fa_collateral(token_owner, token_to_umwrap_address, env_wrapper_data, env_wrapper_data_mut);

        //move old token to conract
        object::transfer(token_owner, token_to_unwrap, env_wrapper_data.creator);

    let BurnConfig { burn_ref } = move_from<BurnConfig>(token_to_umwrap_address);

    simple_map::remove(&mut env_wrapper_data_mut.token_relation, &token_to_umwrap_address);
    token::burn(burn_ref);
    }

    public entry fun add_native_collateral(
        token_owner: &signer,
        token_address: address,
        value: u64) acquires EnvWrapperDataMut, EnvWrapperData
    {
        let token_owner_address = signer::address_of(token_owner);
        assert!(coin::balance<AptosCoin>(token_owner_address) >= value ,error::invalid_argument(ECOIN_NOT_ENOUGH));
		if (value > 0) {
            let env_wrapper_data = borrow_global<EnvWrapperData>(@env_address);
            coin::transfer<AptosCoin>(token_owner, env_wrapper_data.creator, value);
            let env_wrapper_data_mut = borrow_global_mut<EnvWrapperDataMut>(@env_address);
            let w_token_data = simple_map::borrow_mut(&mut env_wrapper_data_mut.token_relation, &token_address);
            let new_value =  w_token_data.backed_value + value;
            
            
            w_token_data.backed_value = new_value;
            event::emit(
                NativeCollateralAdd {
                token: token_address,
                add: value,
                result: new_value,
                });
        };
    }

///Colaterial

    public entry fun  add_fa_collateral(
        token_owner: &signer,
        wrapped_token_address: address, 
        metadata_address: address, 
        amount: u64) acquires EnvWrapperData, EnvWrapperDataMut
    {
        

        let env_wrapper_data = borrow_global_mut<EnvWrapperData>(@env_address);
        let env_wrapper_data_mut = borrow_global_mut<EnvWrapperDataMut>(@env_address);

        assert!(simple_map::contains_key(&mut env_wrapper_data_mut.token_relation, &wrapped_token_address), error::invalid_argument(ETOKEN_ID_NOT_FOUND));
        assert!(simple_map::contains_key(&mut env_wrapper_data.partners_token_list, &metadata_address), error::invalid_argument(ECOLLECTION_NOT_FOUND));
        let partners_token = simple_map::borrow(&mut env_wrapper_data.partners_token_list, &metadata_address);
        assert!(partners_token.enabled_for_collateral, error::invalid_argument(ECOLLECTION_IN_BLACKLIST));

        let token_owner_address = signer::address_of(token_owner);
        let metadata = object::address_to_object<fungible_asset::Metadata>(metadata_address);
        let token_owner_fungible_store = primary_fungible_store::ensure_primary_store_exists(token_owner_address, metadata);
        let balance_fa_token_owner = fungible_asset::balance(token_owner_fungible_store);
        assert!(balance_fa_token_owner >= amount, error::invalid_argument(ELOW_FA_BALANCE));
        
        let this_fungible_store = primary_fungible_store::ensure_primary_store_exists(@env_address, metadata);
        fungible_asset::transfer(token_owner, token_owner_fungible_store, this_fungible_store, amount);
        
        //store data to account
        let old_amount = 0;
        let env_wrapper_data_mut = borrow_global_mut<EnvWrapperDataMut>(@env_address);
        if (simple_map::contains_key(&mut env_wrapper_data_mut.collaterial_relation, &wrapped_token_address)) {
            let collaterial_relation = simple_map::borrow(&mut env_wrapper_data_mut.collaterial_relation, &wrapped_token_address);
            
            let idx = 0;
            let idx_find = false;
            let collaterial_relation_length = vector::length(collaterial_relation);

            loop {
                let add_fa_collateral = vector::borrow(collaterial_relation, idx);
                if (add_fa_collateral.metadata_address == metadata_address) {
                    idx_find = true;
                    break
                };
                idx = idx +1;
                if (idx >= collaterial_relation_length) break;
            };

            let collaterial_relation_mut = simple_map::borrow_mut(&mut env_wrapper_data_mut.collaterial_relation, &wrapped_token_address);
            if (idx_find) {
            let add_fa_collateral_mut = vector::borrow_mut(collaterial_relation_mut, idx);
                old_amount = add_fa_collateral_mut.amount;
                add_fa_collateral_mut.amount = add_fa_collateral_mut.amount + amount; 
            }
            else {
                assert!(collaterial_relation_length < env_wrapper_data.max_fa_count, error::invalid_argument(ETOO_MUCH_COLLATERIAL));
                vector::push_back(collaterial_relation_mut, FaCollateral{metadata_address , amount});
            }
        }
        else {
            let new_collaterial_relations = vector::empty<FaCollateral>(); 
            vector::push_back(&mut new_collaterial_relations, FaCollateral{metadata_address , amount});
            simple_map::add(&mut env_wrapper_data_mut.collaterial_relation, wrapped_token_address, new_collaterial_relations); 

        };

        event::emit(
                FACollateralAdd {
                token: wrapped_token_address,
                metadata: metadata_address,
                add: amount,
                result: old_amount+amount,
                });

    }

    fun return_fa_collateral(token_owner: &signer, token_address: address, env_wrapper_data:&EnvWrapperData, env_wrapper_data_mut: &mut EnvWrapperDataMut) 
    {

        let resource_signer = account::create_signer_with_capability(&env_wrapper_data.signer_cap);
   
        let token_owner_address = signer::address_of(token_owner);

        if (!simple_map::contains_key(&mut env_wrapper_data_mut.collaterial_relation, &token_address)) {return};
        let collaterial_relation = simple_map::borrow(&mut env_wrapper_data_mut.collaterial_relation, &token_address);

        let collaterial_relation_length = vector::length(collaterial_relation);
        if (collaterial_relation_length == 0) {return};
        let idx = 0;
        loop {
            let add_fa_collateral = vector::borrow(collaterial_relation, idx);
            let metadata = object::address_to_object<fungible_asset::Metadata>(add_fa_collateral.metadata_address);
            let this_fungible_store = primary_fungible_store::ensure_primary_store_exists(@env_address, metadata);
            let reciever_fungible_store = primary_fungible_store::ensure_primary_store_exists(token_owner_address, metadata);
			fungible_asset::transfer(&resource_signer, this_fungible_store, reciever_fungible_store, add_fa_collateral.amount);
			idx = idx +1;
            if (idx >= collaterial_relation_length) break;
        };

        simple_map::remove(&mut env_wrapper_data_mut.collaterial_relation, &token_address);

    }


    // Admin-only functions


    public entry fun set_collateral_status(admin: &signer, metadata_address: address, is_enabled: bool) acquires EnvWrapperData {
        assert_is_admin(signer::address_of(admin));
        let _metadata = object::address_to_object<fungible_asset::Metadata>(metadata_address);
        let env_wrapper_data = borrow_global_mut<EnvWrapperData>(@env_address);
            event::emit(
                FACollateralStatusChanged {
                metadata: metadata_address,
                newStatus: is_enabled
                });
        simple_map::upsert(&mut env_wrapper_data.partners_token_list, metadata_address, ListItem {enabled_for_collateral: is_enabled, disabled_for_wrap: false}); 

    }

    public entry fun set_max_fa_collateral_count(admin: &signer, count: u64) acquires EnvWrapperData {
        assert_is_admin(signer::address_of(admin));
        let env_wrapper_data = borrow_global_mut<EnvWrapperData>(@env_address);
 
            event::emit(
                MaxCollateralCountChanged {
                oldValue: env_wrapper_data.max_fa_count,
                newValue: count,
                });
        env_wrapper_data.max_fa_count = count;
    }

    public entry fun edit_partners_item (
        admin: &signer,
        contract: address, 
        is_collateral: bool, 
        is_black_listed: bool) acquires EnvWrapperData
    {
        assert_is_admin(signer::address_of(admin));
        let env_wrapper_data = borrow_global_mut<EnvWrapperData>(@env_address);
        simple_map:: upsert(&mut env_wrapper_data.partners_token_list, contract, ListItem {enabled_for_collateral: is_collateral, disabled_for_wrap: is_black_listed}); 
        event::emit(
        PartnersListChange{
        contract: contract,
        enabled_for_collateral: is_collateral,
        disabled_for_wrap: is_black_listed,
        });
    }

    fun issue_collection(creator: &signer) acquires EnvWrapperData {
//        assert_is_admin(signer::address_of(creator));

        let env_wrapper_data = borrow_global_mut<EnvWrapperData>(@env_address);
        let resource_signer = account::create_signer_with_capability(&env_wrapper_data.signer_cap);

        collection::create_unlimited_collection(
            &resource_signer,
            env_wrapper_data.collection_description,
            env_wrapper_data.collection_name,
            option::none(),
            env_wrapper_data.collection_uri,
        );
        
    }

    public entry fun set_admin(admin: &signer, admin_addr: address) acquires EnvWrapperData {
        let addr = signer::address_of(admin);
        assert_is_admin(addr);
        borrow_global_mut<EnvWrapperData>(@env_address).admin_addr = admin_addr;
    }

    fun mint_nft(uri: String) : Object<Token> acquires EnvWrapperData {
        
        let env_wrapper_data = borrow_global_mut<EnvWrapperData>(@env_address);
        
        env_wrapper_data.minted_supply = env_wrapper_data.minted_supply + 1;
        let resource_signer = account::create_signer_with_capability(&env_wrapper_data.signer_cap);
        
        let name = env_wrapper_data.token_name_base;
        string::append(&mut name, u64_to_string(env_wrapper_data.minted_supply));

        let token_constructor_ref = token::create_from_account(
        &resource_signer,
        env_wrapper_data.collection_name,
        utf8(b""),
        name,
        option::none(),
        uri,
        );
        let burn_ref = token::generate_burn_ref(&token_constructor_ref);
        let token_signer = object::generate_signer(&token_constructor_ref);
      
      move_to(
         &token_signer,
         BurnConfig {
            burn_ref,
         }
      );
 
        // Transfers the token.

        object::object_from_constructor_ref<Token>(&token_constructor_ref)
    }



    fun u64_to_string(value: u64): string::String {
        if (value == 0) {
            return utf8(b"0")
        };
        let buffer = vector::empty<u8>();
        while (value != 0) {
            vector::push_back(&mut buffer, ((48 + value % 10) as u8));
            value = value / 10;
        };
        vector::reverse(&mut buffer);
        utf8(buffer)
    }



    // ------------------------------------ views ------------------------------------
    #[view]
    public fun create_address_for_last_object_account( source: &signer):address   {
        let addr = signer::address_of(source);
        let creation_num = account::get_guid_next_creation_num(addr)-1;
        let guid_id = guid::create_id(addr, creation_num);
        let guid = GUID {
            id: guid_id
        };
        let bytes = bcs::to_bytes(&guid);
        let object_from_guid_address_scheme: u8 = 0xFD; 
        vector::push_back(&mut bytes, object_from_guid_address_scheme);
        let obj_addr = from_bcs::to_address(hash::sha3_256(bytes));
        obj_addr
    }

    #[view]
    public fun token_URI(token_address: address): String  {
        let token = object::address_to_object<Token>(token_address); 
        token::uri(token)
    }
    

    #[view]
    public fun get_token_value(token_address: address): u64  acquires EnvWrapperDataMut {
            let env_wrapper_data_mut = borrow_global_mut<EnvWrapperDataMut>(@env_address);
            let w_token_data = simple_map::borrow_mut(&mut env_wrapper_data_mut.token_relation, &token_address);
            w_token_data.backed_value 

    }

    #[view]
    public fun get_wrapped_token(token_address: address): WTokenData  acquires EnvWrapperDataMut {
            let env_wrapper_data_mut = borrow_global_mut<EnvWrapperDataMut>(@env_address);
            let w_token_data = simple_map::borrow(&mut env_wrapper_data_mut.token_relation, &token_address);
            *w_token_data 
    }


    #[view]
    public fun get_FA_collateral(token_address: address): vector<FaCollateral>  acquires EnvWrapperDataMut {
            if (!exists<EnvWrapperDataMut>(@env_address)) { return vector::empty<FaCollateral>()};
            let env_wrapper_data_mut = borrow_global_mut<EnvWrapperDataMut>(@env_address);
            *simple_map::borrow(&mut env_wrapper_data_mut.collaterial_relation, &token_address)
    }

    #[view]
    public fun get_FA_amount(_token_wrap_address: address, idx:u64): u64  acquires EnvWrapperDataMut{
        let vector_fa_collateral = get_FA_collateral(_token_wrap_address);
        let _fa_collateral = vector::borrow(&vector_fa_collateral, idx);
        _fa_collateral.amount
    }

    #[view]
    public fun get_FA_m_address(_token_wrap_address: address, idx:u64): address  acquires EnvWrapperDataMut{
                let vector_fa_collateral = get_FA_collateral(_token_wrap_address);
        let _fa_collateral = vector::borrow(&vector_fa_collateral, idx);
        _fa_collateral.metadata_address
    }

    #[view]
    public fun get_FA_collateral_balance(token_address: address,fa_address: address): u64  acquires EnvWrapperDataMut {
            let env_wrapper_data_mut = borrow_global_mut<EnvWrapperDataMut>(@env_address);
            let collaterial_relation = simple_map::borrow(&mut env_wrapper_data_mut.collaterial_relation, &token_address);
            let collaterial_relation_length = vector::length(collaterial_relation);
            if (collaterial_relation_length == 0) {return 0};

            let idx = 0;
            loop {
                let add_fa_collateral = vector::borrow(collaterial_relation, idx);
                if (add_fa_collateral.metadata_address==fa_address) return  add_fa_collateral.amount;
                idx = idx +1;
                if (idx > collaterial_relation_length) return 0;
            }
    }

    #[view]
    public fun get_partners_token_list(): SimpleMap<address, ListItem>  acquires EnvWrapperData {
            let env_wrapper_data = borrow_global<EnvWrapperData>(@env_address);
            env_wrapper_data.partners_token_list
    }
    // ------------------------------------ tests ------------------------------------

    #[test_only]
    use aptos_framework::aptos_account::create_account;
//	const TEST_START_TIME: u64 = 1000000000;
   

    #[test_only]
    public fun test_setup(source: &signer, resource_acc: &signer, _framework: &signer, user: &signer)  acquires EnvWrapperData {
        create_account(signer::address_of(source));
        create_account(signer::address_of(user));
        resource_account::create_resource_account(source, (vector[2u8]: vector<u8>), vector::empty());

        init_module(resource_acc);
    }

    #[test_only]
    public entry fun is_enabled_for_collateral(item:ListItem):bool   {
        return item.enabled_for_collateral
    }

    #[test_only]
    public entry fun print_address_resource_acc( _source: &signer)   {

        let resource_addr = account::create_resource_address(&signer::address_of(_source), (vector[2u8]: vector<u8>));
        aptos_std::debug::print<address>(&resource_addr);
        
    }

    #[test_only]
    public entry fun check_wrap_da_data(token_wrap_address: address,token_address: address, unwrap_after: u64, backed_value: u64) acquires EnvWrapperDataMut  {
		let wrap_data=get_wrapped_token(token_wrap_address);
        assert!(wrap_data.addr==token_address, error::invalid_argument(ECHECK_WRAP_DATA));
        assert!(wrap_data.unwrap_after==unwrap_after, error::invalid_argument(ECHECK_WRAP_DATA));
        assert!(wrap_data.backed_value==backed_value, error::invalid_argument(ECHECK_WRAP_DATA));
    }    

    #[test_only]
    public entry fun check_native_collateral_event(token: address, add: u64, result: u64)   {
        let nativeCollateralAdd = NativeCollateralAdd {token, add, result,};
        assert!(event::was_event_emitted<NativeCollateralAdd>(&nativeCollateralAdd), error::invalid_argument(ECHECK_NATIVE_COLLATERAL_EVENT));
    }
    
    #[test_only]
    public entry fun check_fa_collateral_event(token: address, metadata: address, add: u64, result: u64)   {
        let nativeCollateralAdd = FACollateralAdd {token, metadata, add, result,};
        assert!(event::was_event_emitted<FACollateralAdd>(&nativeCollateralAdd), error::invalid_argument(ECHECK_FA_COLLATERAL_EVENT));
    }

    #[test_only]
    public entry fun debug_native_collateral_event()   {
        let native_events = event::emitted_events<NativeCollateralAdd>();
        let native_events_length = vector::length<NativeCollateralAdd>(&native_events);
        aptos_std::debug::print<vector<NativeCollateralAdd>>(&native_events);
        aptos_std::debug::print<u64>(&native_events_length);

    }

    #[test(framework = @0x1, admin = @admin, _source = @source, resource_acc = @env_address, buyer = @0x1234)]
    public entry fun normal_process( _source: &signer)  {
    print_address_resource_acc(_source);
    }
    

}