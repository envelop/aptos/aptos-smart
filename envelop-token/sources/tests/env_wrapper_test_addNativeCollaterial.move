#[test_only]
module env_address::env_wrapper_test_addNativeCollaterial {
    use env_address::env_wrapper;
    use tech_nft_address::tech_nft;
    use da_token::da_token;
    use std::signer;
    use std::string;
    use std::error;
    use std::vector;
    use aptos_framework::timestamp;
    use aptos_framework::object;
    use aptos_framework::aptos_coin;
    use aptos_framework::coin;
    use aptos_framework::event;
    use std::aptos_coin::AptosCoin;
    use aptos_framework::aptos_account::create_account;
	
    // ------------------------------------ tests ------------------------------------

    #[test_only]
	const TEST_START_TIME: u64 = 1000000000;
    const START_NATIVE_COLLATERAL: u64 = 10;
    const ADD_NATIVE_COLLATERAL: u64 = 20;
    const ENOT_EQUAL_COIN: u64 = 1;
    
    #[test_only]
    fun makeNFTForTest(_framework: &signer, _admin: &signer, _source: &signer, resource_acc_tech_nft: &signer, user: &signer):address    {
        tech_nft::mint_nft(user);
        tech_nft::create_address_for_last_object_account(resource_acc_tech_nft)
    }

    #[test_only]
    public entry fun test_setup(_framework: &signer, _admin: &signer, _source: &signer, _resource_acc: &signer, _resource_acc_tech_nft: &signer, _user: &signer, _buyer: &signer)   {

    	timestamp::set_time_has_started_for_testing(_framework);
        timestamp::update_global_time_for_test_secs(TEST_START_TIME);
		env_wrapper::test_setup(_source, _resource_acc, _framework, _user);
        

        da_token::initialize_collection(_admin);
        tech_nft::test_setup(_source, _resource_acc_tech_nft, _framework, _user);
        

        let (burn, mint) = aptos_coin::initialize_for_test(_framework);
        let coins = coin::mint<AptosCoin>(1000000000, &mint);
        coin::deposit(signer::address_of(_user), coins);
        let coins1 = coin::mint<AptosCoin>(1000000000, &mint);
        create_account(signer::address_of(_buyer));

        coin::deposit(signer::address_of(_buyer), coins1);
        coin::destroy_burn_cap(burn);
        coin::destroy_mint_cap(mint);
        
    }

    #[test(_framework = @0x1, _admin = @source, _source = @source, _resource_acc = @env_address, _resource_acc_tech_nft = @tech_nft_address, user = @0x123, _buyer = @0x1234)]
    #[expected_failure(abort_code = 65538, location = 0000000000000000000000000000000000000000000000000000000000000001::simple_map)]
    fun env_wrapper_test_addNativeCollaterial(_framework: &signer, _admin: &signer, _source: &signer, _resource_acc: &signer, _resource_acc_tech_nft: &signer, user: &signer, _buyer: &signer)  {
        let _user_addr = signer::address_of(user);
        let _resource_acc_addr = signer::address_of(_resource_acc);
        
        test_setup(_framework, _admin, _source, _resource_acc, _resource_acc_tech_nft, user, _buyer);
        let da_token_cref = da_token::mint_to(_admin,string::utf8(b"Token #1"),_user_addr);
        let da_token_object_address = object::address_from_constructor_ref(&da_token_cref);
        let initial_user_coin = coin::balance<AptosCoin>(_user_addr);
        
        env_wrapper::wrap(user, da_token_object_address,0,START_NATIVE_COLLATERAL);
        let _token_wrap_address = env_wrapper::create_address_for_last_object_account(_resource_acc);
        assert!(coin::balance<AptosCoin>(_resource_acc_addr) == START_NATIVE_COLLATERAL,error::invalid_state(ENOT_EQUAL_COIN));
        assert!(initial_user_coin-START_NATIVE_COLLATERAL == coin::balance<AptosCoin>(_user_addr),error::invalid_state(ENOT_EQUAL_COIN));

        env_wrapper::add_native_collateral(user,_token_wrap_address,ADD_NATIVE_COLLATERAL);

        env_wrapper::add_native_collateral(user,_token_wrap_address,0);
        assert!(env_wrapper::get_token_value(_token_wrap_address) == START_NATIVE_COLLATERAL+ADD_NATIVE_COLLATERAL,error::invalid_state(ENOT_EQUAL_COIN));


        env_wrapper::check_native_collateral_event(_token_wrap_address, ADD_NATIVE_COLLATERAL,START_NATIVE_COLLATERAL+ADD_NATIVE_COLLATERAL);
        
        env_wrapper::add_native_collateral(_buyer,_token_wrap_address,ADD_NATIVE_COLLATERAL);
        env_wrapper::add_native_collateral(_buyer,_user_addr,ADD_NATIVE_COLLATERAL);
    }

}