#[test_only]
module env_address::env_wrapper_test_721_with_mint {
    use env_address::env_wrapper;
    use tech_nft_address::tech_nft;
    use da_token::da_token;
    use std::signer;

    use aptos_framework::timestamp;
    use aptos_framework::object;
    use aptos_token_objects::token::{ Token};
    use aptos_framework::aptos_coin;
    use aptos_framework::coin;
    use std::aptos_coin::AptosCoin;
    // ------------------------------------ tests ------------------------------------

    #[test_only]
	const TEST_START_TIME: u64 = 1000000000;
   

    #[test_only]
    fun makeNFTForTest(_framework: &signer, _admin: &signer, _source: &signer, resource_acc_tech_nft: &signer, user: &signer):address    {
        tech_nft::mint_nft(user);

        tech_nft::create_address_for_last_object_account(resource_acc_tech_nft)
    }


    #[test_only]
    public entry fun print_address_resource_acc( _source: &signer)   {

        
    }

    #[test_only]
    public entry fun test_setup(_framework: &signer, _admin: &signer, _source: &signer, _resource_acc: &signer, _resource_acc_tech_nft: &signer, _user: &signer, _buyer: &signer)   {

    	timestamp::set_time_has_started_for_testing(_framework);
        timestamp::update_global_time_for_test_secs(TEST_START_TIME);
		env_wrapper::test_setup(_source, _resource_acc, _framework, _user);
        

        da_token::initialize_collection(_admin);
        tech_nft::test_setup(_source, _resource_acc_tech_nft, _framework, _user);
        

        let (burn, mint) = aptos_coin::initialize_for_test(_framework);
        let coins = coin::mint<AptosCoin>(1000000000, &mint);
        coin::deposit(signer::address_of(_user), coins);
        coin::destroy_burn_cap(burn);
        coin::destroy_mint_cap(mint);
        
    }

    #[test(_framework = @0x1, _admin = @source, _source = @source, _resource_acc = @env_address, _resource_acc_tech_nft = @tech_nft_address, user = @0x123, _buyer = @0x12345)]
    fun test_simple_wrap(_framework: &signer, _admin: &signer, _source: &signer, _resource_acc: &signer, _resource_acc_tech_nft: &signer, user: &signer, _buyer: &signer)  {
        test_setup(_framework, _admin, _source, _resource_acc, _resource_acc_tech_nft, user, _buyer);
        env_wrapper::wrap_with_mint(user,0,0);
        let token_address = env_wrapper::create_address_for_last_object_account(_resource_acc_tech_nft);
        let token_wrap_address = env_wrapper::create_address_for_last_object_account(_resource_acc);
        env_wrapper::check_wrap_da_data(token_wrap_address,token_address,0,0);

    }

}