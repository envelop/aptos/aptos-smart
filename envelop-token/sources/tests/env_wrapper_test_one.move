#[test_only]
module env_address::env_wrapper_test_one {
    use env_address::env_wrapper;
    use tech_nft_address::tech_nft;
    use da_token::da_token;
    use std::signer;
//    use std::string;
    use std::error;
//    use std::vector;
//    use std::option;

//    use aptos_framework::aptos_coin::{AptosCoin};
//    use aptos_framework::account;
//    use aptos_framework::coin;
//    use aptos_framework::event;
//    use aptos_framework::resource_account;
    use aptos_framework::timestamp;
    use aptos_framework::object::{Self, Object};
   use aptos_token_objects::token::{Self, Token};
//    use aptos_token_objects::collection;
    use aptos_framework::aptos_coin;
    use aptos_framework::coin;
    use std::aptos_coin::AptosCoin;
    use aptos_framework::aptos_account::create_account;
    use fa_token::fa_token;
    use fa_token1::fa_token1;
    use fa_token2::fa_token2;
    use aptos_framework::fungible_asset;
    use aptos_framework::primary_fungible_store;
	
    // ------------------------------------ tests ------------------------------------

    #[test_only]
	const TEST_START_TIME: u64 = 1000000000;
    const START_NATIVE_COLLATERAL: u64 = 10;
    const ADD_NATIVE_COLLATERAL: u64 = 20;
    const ERC20_COLLATERAL_AMOUNT : u64 = 20000;
    const ENOT_EQUAL_COIN: u64 = 1;
    const ENOT_EQUAL_FA_TOKEN: u64 = 2;
    const ENOT_EQUAL_META_ADDRESS: u64 = 3;
    const ENOT_EQUAL_URI: u64 = 4;
    const ENOT_EQUAL_OWNER_ADDRESS: u64 = 5;
    


    #[test_only]
    fun makeNFTForTest(_framework: &signer, _admin: &signer, _source: &signer, resource_acc_tech_nft: &signer, user: &signer):address    {
        tech_nft::mint_nft(user);
        tech_nft::create_address_for_last_object_account(resource_acc_tech_nft)
    }

    #[test_only]
    fun fa_balance(_user_addr: address, metadata: Object<fungible_asset::Metadata>):u64    {
        let user_fungible_store = primary_fungible_store::ensure_primary_store_exists(_user_addr, metadata);
        fungible_asset::balance(user_fungible_store)
    }



    #[test_only]
    public entry fun test_setup(_framework: &signer, _admin: &signer, _source: &signer, _resource_acc: &signer, _resource_acc_tech_nft: &signer, _user: &signer, _buyer: &signer)   {

    	timestamp::set_time_has_started_for_testing(_framework);
        timestamp::update_global_time_for_test_secs(TEST_START_TIME);
		env_wrapper::test_setup(_source, _resource_acc, _framework, _user);
        

        da_token::initialize_collection(_admin);
        tech_nft::test_setup(_source, _resource_acc_tech_nft, _framework, _user);
        

        let (burn, mint) = aptos_coin::initialize_for_test(_framework);
        let coins = coin::mint<AptosCoin>(1000000000, &mint);
        coin::deposit(signer::address_of(_user), coins);
        let coins1 = coin::mint<AptosCoin>(1000000000, &mint);
        create_account(signer::address_of(_buyer));

        coin::deposit(signer::address_of(_buyer), coins1);
        coin::destroy_burn_cap(burn);
        coin::destroy_mint_cap(mint);

        fa_token::test_setup(_admin);
        fa_token1::test_setup(_admin);
        fa_token2::test_setup(_admin);
        
    }


        #[test(_framework = @0x1, _admin = @source, _source = @source, _resource_acc = @env_address, _resource_acc_tech_nft = @tech_nft_address, user = @0x123, _buyer = @0x12345)]
    fun test_simple_wrap(_framework: &signer, _admin: &signer, _source: &signer, _resource_acc: &signer, _resource_acc_tech_nft: &signer, user: &signer, _buyer: &signer)  {
        test_setup(_framework, _admin, _source, _resource_acc, _resource_acc_tech_nft, user, _buyer);
        let token_address = makeNFTForTest(_framework, _admin, _source, _resource_acc_tech_nft, user);
        env_wrapper::wrap(user, token_address,0,0);
        let token_wrap_address = env_wrapper::create_address_for_last_object_account(_resource_acc);
        env_wrapper::check_wrap_da_data(token_wrap_address,token_address,0,0);

    }


        #[test(_framework = @0x1, _admin = @source, _source = @source, _resource_acc = @env_address, _resource_acc_tech_nft = @tech_nft_address, user = @0x123, _buyer = @0x12345)]
    fun test_check_uri(_framework: &signer, _admin: &signer, _source: &signer, _resource_acc: &signer, _resource_acc_tech_nft: &signer, user: &signer, _buyer: &signer)  {
        test_setup(_framework, _admin, _source, _resource_acc, _resource_acc_tech_nft, user, _buyer);
        let token_address = makeNFTForTest(_framework, _admin, _source, _resource_acc_tech_nft, user);
        let minted_token = object::address_to_object<Token>(token_address);
        let token_uri = token::uri<Token>(minted_token);
        env_wrapper::wrap(user, token_address,0,0);
        let token_wrap_address = env_wrapper::create_address_for_last_object_account(_resource_acc);
        let token_wrap = object::address_to_object<Token>(token_wrap_address);
        assert!(token_uri == token::uri<Token>(token_wrap), error::invalid_state(ENOT_EQUAL_URI));
   
    }

    #[test(_framework = @0x1, _admin = @source, _source = @source, _resource_acc = @env_address, _resource_acc_tech_nft = @tech_nft_address, user = @0x123, _buyer = @0x12345)]
    fun test_simple_unwrap(_framework: &signer, _admin: &signer, _source: &signer, _resource_acc: &signer, _resource_acc_tech_nft: &signer, user: &signer, _buyer: &signer)  {
        test_setup(_framework, _admin, _source, _resource_acc, _resource_acc_tech_nft, user, _buyer);
        let token_address = makeNFTForTest(_framework, _admin, _source, _resource_acc_tech_nft, user);
        env_wrapper::wrap(user, token_address,0,0);
        let token_wrap_address = env_wrapper::create_address_for_last_object_account(_resource_acc);
        env_wrapper::check_wrap_da_data(token_wrap_address,token_address,0,0);
        env_wrapper::unwrap(user, token_wrap_address);
        
    }


    #[test(_framework = @0x1, _admin = @source, _source = @source, _resource_acc = @env_address, _resource_acc_tech_nft = @tech_nft_address, user = @0x123, _buyer = @0x12345)]
    fun test_ether_wrap(_framework: &signer, _admin: &signer, _source: &signer, _resource_acc: &signer, _resource_acc_tech_nft: &signer, user: &signer, _buyer: &signer)  {
        test_setup(_framework, _admin, _source, _resource_acc, _resource_acc_tech_nft, user, _buyer);
        let token_address = makeNFTForTest(_framework, _admin, _source, _resource_acc_tech_nft, user);
        env_wrapper::wrap(user, token_address,0,START_NATIVE_COLLATERAL);
        let token_wrap_address = env_wrapper::create_address_for_last_object_account(_resource_acc);
        env_wrapper::check_wrap_da_data(token_wrap_address,token_address,0,START_NATIVE_COLLATERAL);

        let _resource_acc_addr = signer::address_of(_resource_acc);
        assert!(object::owner<Token>(object::address_to_object<Token>(token_address)) == _resource_acc_addr, error::invalid_state(ENOT_EQUAL_OWNER_ADDRESS));
        assert!(coin::balance<AptosCoin>(_resource_acc_addr) == START_NATIVE_COLLATERAL,error::invalid_state(ENOT_EQUAL_COIN));
        assert!(env_wrapper::get_token_value(token_wrap_address) == START_NATIVE_COLLATERAL,error::invalid_state(ENOT_EQUAL_COIN));

    }

    #[test(_framework = @0x1, _admin = @source, _source = @source, _resource_acc = @env_address, _resource_acc_tech_nft = @tech_nft_address, user = @0x123, _buyer = @0x12345)]
    fun test_add_collateral(_framework: &signer, _admin: &signer, _source: &signer, _resource_acc: &signer, _resource_acc_tech_nft: &signer, user: &signer, _buyer: &signer)  {
        test_setup(_framework, _admin, _source, _resource_acc, _resource_acc_tech_nft, user, _buyer);
        let token_address = makeNFTForTest(_framework, _admin, _source, _resource_acc_tech_nft, user);
        env_wrapper::wrap(user, token_address,0,0);
        let token_wrap_address = env_wrapper::create_address_for_last_object_account(_resource_acc);
        let native_collateral_before = env_wrapper::get_token_value(token_wrap_address);
        env_wrapper::add_native_collateral(user,token_wrap_address, ADD_NATIVE_COLLATERAL);
        assert!((env_wrapper::get_token_value(token_wrap_address)-native_collateral_before) == ADD_NATIVE_COLLATERAL, error::invalid_state(ENOT_EQUAL_COIN));
        
    }


    #[test(_framework = @0x1, _admin = @source, _source = @source, _resource_acc = @env_address, _resource_acc_tech_nft = @tech_nft_address, user = @0x123, _buyer = @0x12345)]
    fun test_ether_unwrap(_framework: &signer, _admin: &signer, _source: &signer, _resource_acc: &signer, _resource_acc_tech_nft: &signer, user: &signer, _buyer: &signer)  {
        test_setup(_framework, _admin, _source, _resource_acc, _resource_acc_tech_nft, user, _buyer);
        let token_address = makeNFTForTest(_framework, _admin, _source, _resource_acc_tech_nft, user);
        env_wrapper::wrap(user, token_address,0,0);
        let token_wrap_address = env_wrapper::create_address_for_last_object_account(_resource_acc);

        env_wrapper::add_native_collateral(user,token_wrap_address, ADD_NATIVE_COLLATERAL);

        let _user_addr = signer::address_of(user);
        let _buyer_addr = signer::address_of(_buyer);

        object::transfer_raw(user, token_wrap_address,_buyer_addr);
        object::transfer_raw(_buyer, token_wrap_address,_user_addr);
        object::transfer_raw(user, token_wrap_address,_buyer_addr);
        object::transfer_raw(_buyer, token_wrap_address,_user_addr);

        let eth_before = coin::balance<AptosCoin>(_user_addr);
        let token_value = env_wrapper::get_token_value(token_wrap_address);
        env_wrapper::unwrap(user, token_wrap_address);

        let _resource_acc_addr = signer::address_of(_resource_acc);

        assert!(object::owner<Token>(object::address_to_object<Token>(token_address)) == _user_addr, error::invalid_state(ENOT_EQUAL_OWNER_ADDRESS));
        assert!(coin::balance<AptosCoin>(_resource_acc_addr) == 0, error::invalid_state(ENOT_EQUAL_COIN));
        assert!((coin::balance<AptosCoin>(_user_addr)-eth_before) == token_value, error::invalid_state(ENOT_EQUAL_COIN));

    }



    #[test(_framework = @0x1, _admin = @source, _source = @source, _resource_acc = @env_address, _resource_acc_tech_nft = @tech_nft_address, user = @0x123, _buyer = @0x12345)]
    #[expected_failure(abort_code = 65544, location = env_address::env_wrapper)]
    fun test_advanced_wrap_bad_time(_framework: &signer, _admin: &signer, _source: &signer, _resource_acc: &signer, _resource_acc_tech_nft: &signer, user: &signer, _buyer: &signer)  {
        test_setup(_framework, _admin, _source, _resource_acc, _resource_acc_tech_nft, user, _buyer);
        let token_address = makeNFTForTest(_framework, _admin, _source, _resource_acc_tech_nft, user);
        env_wrapper::wrap(user, token_address,TEST_START_TIME+100,START_NATIVE_COLLATERAL);
        let token_wrap_address = env_wrapper::create_address_for_last_object_account(_resource_acc);

        let _user_addr = signer::address_of(user);
        let _buyer_addr = signer::address_of(_buyer);

        object::transfer_raw(user, token_wrap_address,_buyer_addr);

        env_wrapper::unwrap(_buyer, token_wrap_address);

    }



    #[test(_framework = @0x1, _admin = @source, _source = @source, _resource_acc = @env_address, _resource_acc_tech_nft = @tech_nft_address, user = @0x123, _buyer = @0x12345)]
    fun test_advanced_wrap(_framework: &signer, _admin: &signer, _source: &signer, _resource_acc: &signer, _resource_acc_tech_nft: &signer, user: &signer, _buyer: &signer)  {
        test_setup(_framework, _admin, _source, _resource_acc, _resource_acc_tech_nft, user, _buyer);
        let token_address = makeNFTForTest(_framework, _admin, _source, _resource_acc_tech_nft, user);
        env_wrapper::wrap(user, token_address,TEST_START_TIME+100,START_NATIVE_COLLATERAL);
        let token_wrap_address = env_wrapper::create_address_for_last_object_account(_resource_acc);


        let _user_addr = signer::address_of(user);
        let _buyer_addr = signer::address_of(_buyer);



        env_wrapper::add_native_collateral(user,token_wrap_address, ADD_NATIVE_COLLATERAL);

        let metadata = fa_token::get_metadata(_admin);
        let metadata_address = object::object_address<fungible_asset::Metadata>(&metadata);
        fa_token::mint(_admin,_user_addr,ERC20_COLLATERAL_AMOUNT);

        let metadata1 = fa_token1::get_metadata(_admin);
        let metadata_address1 = object::object_address<fungible_asset::Metadata>(&metadata1);
        fa_token1::mint(_admin,_user_addr,ERC20_COLLATERAL_AMOUNT);

        env_wrapper::set_collateral_status(_admin,metadata_address, true);
        env_wrapper::set_collateral_status(_admin,metadata_address1, true);

        env_wrapper::add_fa_collateral(user, token_wrap_address, metadata_address, ERC20_COLLATERAL_AMOUNT);
        env_wrapper::add_fa_collateral(user, token_wrap_address, metadata_address1, ERC20_COLLATERAL_AMOUNT);


        object::transfer_raw(user, token_wrap_address,_buyer_addr);
        let ethBefore = coin::balance<AptosCoin>(_buyer_addr);

        timestamp::update_global_time_for_test_secs(TEST_START_TIME+100);

        env_wrapper::unwrap(_buyer, token_wrap_address);
        assert!(coin::balance<AptosCoin>(_buyer_addr) == ethBefore+ADD_NATIVE_COLLATERAL+START_NATIVE_COLLATERAL, error::invalid_state(ENOT_EQUAL_COIN));
        assert!(fa_balance(_buyer_addr, metadata) == ERC20_COLLATERAL_AMOUNT,error::invalid_state(ENOT_EQUAL_FA_TOKEN));
        assert!(fa_balance(_buyer_addr, metadata1) == ERC20_COLLATERAL_AMOUNT,error::invalid_state(ENOT_EQUAL_FA_TOKEN));


    }


}