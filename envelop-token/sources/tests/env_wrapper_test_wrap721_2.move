#[test_only]
module env_address::env_wrapper_test_721_2 {
    use env_address::env_wrapper;
    use tech_nft_address::tech_nft;
    use da_token::da_token;
    use std::signer;
    use std::string;

    use aptos_framework::timestamp;
    use aptos_framework::object;
    use aptos_token_objects::token::{Self, Token};
    use aptos_token_objects::collection;
    use aptos_framework::aptos_coin;
    use aptos_framework::coin;
    use std::aptos_coin::AptosCoin;
    // ------------------------------------ tests ------------------------------------

    #[test_only]
	const TEST_START_TIME: u64 = 1000000000;
    #[test_only]
    fun makeNFTForTest(_framework: &signer, _admin: &signer, _source: &signer, resource_acc_tech_nft: &signer, user: &signer):address    {
        tech_nft::mint_nft(user);
        tech_nft::create_address_for_last_object_account(resource_acc_tech_nft)
    }

    #[test_only]
    public entry fun test_setup(_framework: &signer, _admin: &signer, _source: &signer, _resource_acc: &signer, _resource_acc_tech_nft: &signer, _user: &signer, _buyer: &signer)   {

    	timestamp::set_time_has_started_for_testing(_framework);
        timestamp::update_global_time_for_test_secs(TEST_START_TIME);
		env_wrapper::test_setup(_source, _resource_acc, _framework, _user);
        

        da_token::initialize_collection(_admin);
        tech_nft::test_setup(_source, _resource_acc_tech_nft, _framework, _user);
        

        let (burn, mint) = aptos_coin::initialize_for_test(_framework);
        let coins = coin::mint<AptosCoin>(1000000000, &mint);
        coin::deposit(signer::address_of(_user), coins);
        coin::destroy_burn_cap(burn);
        coin::destroy_mint_cap(mint);
        
    }


    #[test(_framework = @0x1, _admin = @source, _source = @source, _resource_acc = @env_address, _resource_acc_tech_nft = @tech_nft_address, user = @0x123, _buyer = @0x1234)]
    #[expected_failure(abort_code = 327680, location = env_address::env_wrapper)]
    fun test_edit_partners_item_not_admin(_framework: &signer, _admin: &signer, _source: &signer, _resource_acc: &signer, _resource_acc_tech_nft: &signer, user: &signer, _buyer: &signer)  {
        let _user_addr = signer::address_of(user);
        test_setup(_framework, _admin, _source, _resource_acc, _resource_acc_tech_nft, user, _buyer);
        let tech_token_collection_address = tech_nft::get_collection_address();
        env_wrapper::edit_partners_item(_buyer,tech_token_collection_address,false,false);

    }    

    #[test(_framework = @0x1, _admin = @source, _source = @source, _resource_acc = @env_address, _resource_acc_tech_nft = @tech_nft_address, user = @0x123, _buyer = @0x1234)]
    #[expected_failure(abort_code = 65543, location = env_address::env_wrapper)]
    fun test_wrap_for_blocked_original_nft_contract(_framework: &signer, _admin: &signer, _source: &signer, _resource_acc: &signer, _resource_acc_tech_nft: &signer, user: &signer, _buyer: &signer)  {
        let _user_addr = signer::address_of(user);
        test_setup(_framework, _admin, _source, _resource_acc, _resource_acc_tech_nft, user, _buyer);
        let da_token_cref = da_token::mint_to(_admin,string::utf8(b"Token #1"),_user_addr);
        let da_token_object_address = object::address_from_constructor_ref(&da_token_cref);
        let da_token_object = object::address_to_object<Token>(da_token_object_address); 
        
        let collection_token_to_wrap = token::collection_object<Token>(da_token_object);
        let collection_token_to_wrap_address = object::object_address<collection::Collection>(&collection_token_to_wrap);


        env_wrapper::edit_partners_item(_admin, collection_token_to_wrap_address,false,true);
 
        env_wrapper::wrap(user, da_token_object_address,0,0);

    }


}