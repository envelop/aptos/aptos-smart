#[test_only]
module env_address::env_wrapper_test_addERC20Collateral {
    use env_address::env_wrapper;
//    use env_address::ListItem;
    use tech_nft_address::tech_nft;
    use da_token::da_token;
    use std::signer;
    use std::string;
    use std::error;
    use aptos_framework::timestamp;
    use aptos_framework::object::{Self, Object};
    use aptos_framework::aptos_coin;
    use aptos_framework::coin;
    use std::aptos_coin::AptosCoin;
    use aptos_framework::aptos_account::create_account;
    use fa_token::fa_token;
    use fa_token1::fa_token1;
    use fa_token2::fa_token2;
    use aptos_framework::fungible_asset;
    use aptos_framework::primary_fungible_store;
    use std::simple_map::{SimpleMap,Self};

    // ------------------------------------ tests ------------------------------------

    #[test_only]
	const TEST_START_TIME: u64 = 1000000000;
    const START_NATIVE_COLLATERAL: u64 = 10;
    const ADD_NATIVE_COLLATERAL: u64 = 20;
    const ERC20_COLLATERAL_AMOUNT : u64 = 20000;
    const ENOT_EQUAL_COIN: u64 = 1;
    const ENOT_EQUAL_FA_TOKEN: u64 = 2;
    const ENOT_ENABLED_FOR_COLATERAL: u64 = 3;
 
    #[test_only]
    fun makeNFTForTest(_framework: &signer, _admin: &signer, _source: &signer, resource_acc_tech_nft: &signer, user: &signer):address    {
        tech_nft::mint_nft(user);
        tech_nft::create_address_for_last_object_account(resource_acc_tech_nft)
    }

    #[test_only]
    fun fa_balance(_user_addr: address, metadata: Object<fungible_asset::Metadata>):u64    {
        let user_fungible_store = primary_fungible_store::ensure_primary_store_exists(_user_addr, metadata);
        fungible_asset::balance(user_fungible_store)
    }



    #[test_only]
    public entry fun test_setup(_framework: &signer, _admin: &signer, _source: &signer, _resource_acc: &signer, _resource_acc_tech_nft: &signer, _user: &signer, _buyer: &signer)   {

    	timestamp::set_time_has_started_for_testing(_framework);
        timestamp::update_global_time_for_test_secs(TEST_START_TIME);
		env_wrapper::test_setup(_source, _resource_acc, _framework, _user);
        

        da_token::initialize_collection(_admin);
        tech_nft::test_setup(_source, _resource_acc_tech_nft, _framework, _user);
        

        let (burn, mint) = aptos_coin::initialize_for_test(_framework);
        let coins = coin::mint<AptosCoin>(1000000000, &mint);
        coin::deposit(signer::address_of(_user), coins);
        let coins1 = coin::mint<AptosCoin>(1000000000, &mint);
        create_account(signer::address_of(_buyer));

        coin::deposit(signer::address_of(_buyer), coins1);
        coin::destroy_burn_cap(burn);
        coin::destroy_mint_cap(mint);

        fa_token::test_setup(_admin);
        fa_token1::test_setup(_admin);
        fa_token2::test_setup(_admin);
        
    }

    #[test(_framework = @0x1, _admin = @source, _source = @source, _resource_acc = @env_address, _resource_acc_tech_nft = @tech_nft_address, user = @0x123, _buyer = @0x1234)]
    #[expected_failure(abort_code = 65539, location = env_address::env_wrapper)]
    fun env_wrapper_test_add_bad_ERC20Collaterial(_framework: &signer, _admin: &signer, _source: &signer, _resource_acc: &signer, _resource_acc_tech_nft: &signer, user: &signer, _buyer: &signer)  {
        let _user_addr = signer::address_of(user);
        let _resource_acc_addr = signer::address_of(_resource_acc);
        
        test_setup(_framework, _admin, _source, _resource_acc, _resource_acc_tech_nft, user, _buyer);
        let da_token_cref = da_token::mint_to(_admin,string::utf8(b"Token #1"),_user_addr);
        let da_token_object_address = object::address_from_constructor_ref(&da_token_cref);
        
        env_wrapper::wrap(user, da_token_object_address,0,START_NATIVE_COLLATERAL);
        let _token_wrap_address = env_wrapper::create_address_for_last_object_account(_resource_acc);

        let metadata = fa_token::get_metadata(_admin);
        let metadata_address = object::object_address<fungible_asset::Metadata>(&metadata);

        env_wrapper::add_fa_collateral(user, _token_wrap_address, metadata_address, 0);
    }


    #[test(_framework = @0x1, _admin = @source, _source = @source, _resource_acc = @env_address, _resource_acc_tech_nft = @tech_nft_address, user = @0x123, _buyer = @0x1234)]
    #[expected_failure(abort_code = 65545, location = env_address::env_wrapper)]
   fun env_wrapper_test_addERC20Collaterial_low_balance(_framework: &signer, _admin: &signer, _source: &signer, _resource_acc: &signer, _resource_acc_tech_nft: &signer, user: &signer, _buyer: &signer)  {
        let _user_addr = signer::address_of(user);
        let _resource_acc_addr = signer::address_of(_resource_acc);
        
        test_setup(_framework, _admin, _source, _resource_acc, _resource_acc_tech_nft, user, _buyer);
        let da_token_cref = da_token::mint_to(_admin,string::utf8(b"Token #1"),_user_addr);
        let da_token_object_address = object::address_from_constructor_ref(&da_token_cref);
        
        env_wrapper::wrap(user, da_token_object_address,0,START_NATIVE_COLLATERAL);
        let _token_wrap_address = env_wrapper::create_address_for_last_object_account(_resource_acc);

        let metadata = fa_token::get_metadata(_admin);
        let metadata_address = object::object_address<fungible_asset::Metadata>(&metadata);

        env_wrapper::set_collateral_status(_admin,metadata_address,true);

        let partners_token_list = env_wrapper::get_partners_token_list();
        let partners_token_list_item = *simple_map::borrow(&partners_token_list, &metadata_address);
        assert!(env_wrapper::is_enabled_for_collateral(partners_token_list_item),error::invalid_argument(ENOT_ENABLED_FOR_COLATERAL));

        env_wrapper::add_fa_collateral(user, _token_wrap_address, metadata_address, ERC20_COLLATERAL_AMOUNT);
    }

    #[test(_framework = @0x1, _admin = @source, _source = @source, _resource_acc = @env_address, _resource_acc_tech_nft = @tech_nft_address, user = @0x123, _buyer = @0x1234)]
   fun env_wrapper_test_addERC20Collaterial(_framework: &signer, _admin: &signer, _source: &signer, _resource_acc: &signer, _resource_acc_tech_nft: &signer, user: &signer, _buyer: &signer)  {
        let _user_addr = signer::address_of(user);
        let _resource_acc_addr = signer::address_of(_resource_acc);
        
        test_setup(_framework, _admin, _source, _resource_acc, _resource_acc_tech_nft, user, _buyer);
        let da_token_cref = da_token::mint_to(_admin,string::utf8(b"Token #1"),_user_addr);
        let da_token_object_address = object::address_from_constructor_ref(&da_token_cref);
        
        env_wrapper::wrap(user, da_token_object_address,0,START_NATIVE_COLLATERAL);
        let _token_wrap_address = env_wrapper::create_address_for_last_object_account(_resource_acc);

        let metadata = fa_token::get_metadata(_admin);
        fa_token::mint(_admin,_user_addr,10*ERC20_COLLATERAL_AMOUNT);

        let _buyer_addr = signer::address_of(_buyer);
        fa_token::mint(_admin,_buyer_addr,ERC20_COLLATERAL_AMOUNT);
        let metadata_address = object::object_address<fungible_asset::Metadata>(&metadata);



        env_wrapper::set_collateral_status(_admin,metadata_address,true);
    //First add collaterial
        env_wrapper::add_fa_collateral(user, _token_wrap_address, metadata_address, ERC20_COLLATERAL_AMOUNT);

        assert!(fa_balance(_user_addr, metadata) == 9*ERC20_COLLATERAL_AMOUNT,error::invalid_state(ENOT_EQUAL_FA_TOKEN));
        assert!(fa_balance(_resource_acc_addr, metadata) == ERC20_COLLATERAL_AMOUNT,error::invalid_state(ENOT_EQUAL_FA_TOKEN));                
        assert!(env_wrapper::get_FA_amount(_token_wrap_address,0) == ERC20_COLLATERAL_AMOUNT,error::invalid_state(ENOT_EQUAL_FA_TOKEN));

        env_wrapper::check_fa_collateral_event(_token_wrap_address, metadata_address, ERC20_COLLATERAL_AMOUNT, ERC20_COLLATERAL_AMOUNT);


    //Second add collaterial
        env_wrapper::add_fa_collateral(user, _token_wrap_address, metadata_address, ERC20_COLLATERAL_AMOUNT);

        assert!(fa_balance(_user_addr, metadata) == 8*ERC20_COLLATERAL_AMOUNT,error::invalid_state(ENOT_EQUAL_FA_TOKEN));
        assert!(fa_balance(_resource_acc_addr, metadata) == 2*ERC20_COLLATERAL_AMOUNT,error::invalid_state(ENOT_EQUAL_FA_TOKEN));                
        assert!(env_wrapper::get_FA_amount(_token_wrap_address,0) == 2*ERC20_COLLATERAL_AMOUNT,error::invalid_state(ENOT_EQUAL_FA_TOKEN));
    //Not owner add collaterial
        env_wrapper::add_fa_collateral(_buyer, _token_wrap_address, metadata_address, ERC20_COLLATERAL_AMOUNT);

        assert!(fa_balance(_buyer_addr, metadata) == 0,error::invalid_state(ENOT_EQUAL_FA_TOKEN));
        assert!(fa_balance(_resource_acc_addr, metadata) == 3*ERC20_COLLATERAL_AMOUNT,error::invalid_state(ENOT_EQUAL_FA_TOKEN));                
        assert!(env_wrapper::get_FA_amount(_token_wrap_address,0) == 3*ERC20_COLLATERAL_AMOUNT,error::invalid_state(ENOT_EQUAL_FA_TOKEN));

        let metadata1 = fa_token1::get_metadata(_admin);
        fa_token1::mint(_admin,_buyer_addr,ERC20_COLLATERAL_AMOUNT);
        let metadata_address1 = object::object_address<fungible_asset::Metadata>(&metadata1);
        env_wrapper::set_collateral_status(_admin,metadata_address1,true);

        env_wrapper::add_fa_collateral(_buyer, _token_wrap_address, metadata_address1, ERC20_COLLATERAL_AMOUNT);

        assert!(fa_balance(_buyer_addr, metadata1) == 0,error::invalid_state(ENOT_EQUAL_FA_TOKEN));
        assert!(fa_balance(_resource_acc_addr, metadata1) == ERC20_COLLATERAL_AMOUNT,error::invalid_state(ENOT_EQUAL_FA_TOKEN));                
        assert!(env_wrapper::get_FA_amount(_token_wrap_address,1) == ERC20_COLLATERAL_AMOUNT,error::invalid_state(ENOT_EQUAL_FA_TOKEN));

        env_wrapper::set_max_fa_collateral_count(_admin,2);
    }




    #[test(_framework = @0x1, _admin = @source, _source = @source, _resource_acc = @env_address, _resource_acc_tech_nft = @tech_nft_address, user = @0x123, _buyer = @0x1234)]
    #[expected_failure(abort_code = 65540, location = env_address::env_wrapper)]
   fun env_wrapper_test_addERC20Collaterial_non_exist_token(_framework: &signer, _admin: &signer, _source: &signer, _resource_acc: &signer, _resource_acc_tech_nft: &signer, user: &signer, _buyer: &signer)  {
        let _user_addr = signer::address_of(user);
        let _resource_acc_addr = signer::address_of(_resource_acc);
        
        test_setup(_framework, _admin, _source, _resource_acc, _resource_acc_tech_nft, user, _buyer);
        let da_token_cref = da_token::mint_to(_admin,string::utf8(b"Token #1"),_user_addr);
        let da_token_object_address = object::address_from_constructor_ref(&da_token_cref);
        
        env_wrapper::wrap(user, da_token_object_address,0,START_NATIVE_COLLATERAL);
        let _token_wrap_address = env_wrapper::create_address_for_last_object_account(_resource_acc);

        let metadata = fa_token::get_metadata(_admin);
        fa_token::mint(_admin,_user_addr,10*ERC20_COLLATERAL_AMOUNT);

        let _buyer_addr = signer::address_of(_buyer);
        fa_token::mint(_admin,_buyer_addr,ERC20_COLLATERAL_AMOUNT);
        let metadata_address = object::object_address<fungible_asset::Metadata>(&metadata);



        env_wrapper::set_collateral_status(_admin,metadata_address,true);
    //First add collaterial
        env_wrapper::add_fa_collateral(user, _buyer_addr, metadata_address, ERC20_COLLATERAL_AMOUNT);

    }

    #[test(_framework = @0x1, _admin = @source, _source = @source, _resource_acc = @env_address, _resource_acc_tech_nft = @tech_nft_address, user = @0x123, _buyer = @0x1234)]
    #[expected_failure(abort_code = 327680, location = env_address::env_wrapper)]
   fun env_wrapper_test_addERC20Collaterial_non_admin_set(_framework: &signer, _admin: &signer, _source: &signer, _resource_acc: &signer, _resource_acc_tech_nft: &signer, user: &signer, _buyer: &signer)  {
        let _user_addr = signer::address_of(user);
        let _resource_acc_addr = signer::address_of(_resource_acc);
        
        test_setup(_framework, _admin, _source, _resource_acc, _resource_acc_tech_nft, user, _buyer);
        let da_token_cref = da_token::mint_to(_admin,string::utf8(b"Token #1"),_user_addr);
        let da_token_object_address = object::address_from_constructor_ref(&da_token_cref);
        
        env_wrapper::wrap(user, da_token_object_address,0,START_NATIVE_COLLATERAL);
        let _token_wrap_address = env_wrapper::create_address_for_last_object_account(_resource_acc);

        let metadata = fa_token::get_metadata(_admin);
        fa_token::mint(_admin,_user_addr,10*ERC20_COLLATERAL_AMOUNT);

        let _buyer_addr = signer::address_of(_buyer);
        fa_token::mint(_admin,_buyer_addr,ERC20_COLLATERAL_AMOUNT);
        let metadata_address = object::object_address<fungible_asset::Metadata>(&metadata);

        env_wrapper::set_collateral_status(_buyer,metadata_address,true);

    }


    #[test(_framework = @0x1, _admin = @source, _source = @source, _resource_acc = @env_address, _resource_acc_tech_nft = @tech_nft_address, user = @0x123, _buyer = @0x1234)]
    #[expected_failure(abort_code = 65546, location = env_address::env_wrapper)]
   fun env_wrapper_test_too_mach_collaterial(_framework: &signer, _admin: &signer, _source: &signer, _resource_acc: &signer, _resource_acc_tech_nft: &signer, user: &signer, _buyer: &signer)  {
        let _user_addr = signer::address_of(user);
        let _resource_acc_addr = signer::address_of(_resource_acc);
        
        test_setup(_framework, _admin, _source, _resource_acc, _resource_acc_tech_nft, user, _buyer);
        let da_token_cref = da_token::mint_to(_admin,string::utf8(b"Token #1"),_user_addr);
        let da_token_object_address = object::address_from_constructor_ref(&da_token_cref);
        
        env_wrapper::wrap(user, da_token_object_address,0,START_NATIVE_COLLATERAL);
        let _token_wrap_address = env_wrapper::create_address_for_last_object_account(_resource_acc);

        let metadata = fa_token::get_metadata(_admin);
        fa_token::mint(_admin,_user_addr,10*ERC20_COLLATERAL_AMOUNT);

        let _buyer_addr = signer::address_of(_buyer);
        fa_token::mint(_admin,_buyer_addr,ERC20_COLLATERAL_AMOUNT);
        let metadata_address = object::object_address<fungible_asset::Metadata>(&metadata);



        env_wrapper::set_collateral_status(_admin,metadata_address,true);
    //First add collaterial
        env_wrapper::add_fa_collateral(user, _token_wrap_address, metadata_address, ERC20_COLLATERAL_AMOUNT);

        assert!(fa_balance(_user_addr, metadata) == 9*ERC20_COLLATERAL_AMOUNT,error::invalid_state(ENOT_EQUAL_FA_TOKEN));
        assert!(fa_balance(_resource_acc_addr, metadata) == ERC20_COLLATERAL_AMOUNT,error::invalid_state(ENOT_EQUAL_FA_TOKEN));                
        assert!(env_wrapper::get_FA_amount(_token_wrap_address,0) == ERC20_COLLATERAL_AMOUNT,error::invalid_state(ENOT_EQUAL_FA_TOKEN));
    //Second add collaterial
        env_wrapper::add_fa_collateral(user, _token_wrap_address, metadata_address, ERC20_COLLATERAL_AMOUNT);

        assert!(fa_balance(_user_addr, metadata) == 8*ERC20_COLLATERAL_AMOUNT,error::invalid_state(ENOT_EQUAL_FA_TOKEN));
        assert!(fa_balance(_resource_acc_addr, metadata) == 2*ERC20_COLLATERAL_AMOUNT,error::invalid_state(ENOT_EQUAL_FA_TOKEN));                
        assert!(env_wrapper::get_FA_amount(_token_wrap_address,0) == 2*ERC20_COLLATERAL_AMOUNT,error::invalid_state(ENOT_EQUAL_FA_TOKEN));
    //Not owner add collaterial
        env_wrapper::add_fa_collateral(_buyer, _token_wrap_address, metadata_address, ERC20_COLLATERAL_AMOUNT);

        assert!(fa_balance(_buyer_addr, metadata) == 0,error::invalid_state(ENOT_EQUAL_FA_TOKEN));
        assert!(fa_balance(_resource_acc_addr, metadata) == 3*ERC20_COLLATERAL_AMOUNT,error::invalid_state(ENOT_EQUAL_FA_TOKEN));                
        assert!(env_wrapper::get_FA_amount(_token_wrap_address,0) == 3*ERC20_COLLATERAL_AMOUNT,error::invalid_state(ENOT_EQUAL_FA_TOKEN));

        let metadata1 = fa_token1::get_metadata(_admin);
        fa_token1::mint(_admin,_buyer_addr,ERC20_COLLATERAL_AMOUNT);
        let metadata_address1 = object::object_address<fungible_asset::Metadata>(&metadata1);
        env_wrapper::set_collateral_status(_admin,metadata_address1,true);

        env_wrapper::add_fa_collateral(_buyer, _token_wrap_address, metadata_address1, ERC20_COLLATERAL_AMOUNT);

        assert!(fa_balance(_buyer_addr, metadata1) == 0,error::invalid_state(ENOT_EQUAL_FA_TOKEN));
        assert!(fa_balance(_resource_acc_addr, metadata1) == ERC20_COLLATERAL_AMOUNT,error::invalid_state(ENOT_EQUAL_FA_TOKEN));                
        assert!(env_wrapper::get_FA_amount(_token_wrap_address,1) == ERC20_COLLATERAL_AMOUNT,error::invalid_state(ENOT_EQUAL_FA_TOKEN));

        env_wrapper::set_max_fa_collateral_count(_admin,2);

        let metadata2 = fa_token2::get_metadata(_admin);
        fa_token2::mint(_admin,_buyer_addr,ERC20_COLLATERAL_AMOUNT);
        let metadata_address2 = object::object_address<fungible_asset::Metadata>(&metadata2);
        env_wrapper::set_collateral_status(_admin,metadata_address2,true);

        env_wrapper::add_fa_collateral(_buyer, _token_wrap_address, metadata_address2, ERC20_COLLATERAL_AMOUNT);


    }



}