### Deploy
```
aptos move create-resource-account-and-publish-package --address-name  env_address {--seed X}
```
{--seed X} - optional. It is need when deploy nore than one contract from one user account. 
For this main contact, recomended {--seed 2}, for tech contract {--seed 1}
### Test
```
aptos move test --named-addresses env_address=388fc1dacc3e6b98976b8fdbe690a837f701f0082bb96b46e7643128204550eb
```
